<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserActivityDetail extends Model
{
    use HasFactory;


    protected $fillable = [
        'user_id',
        'user_name',
        'user_contact_no',
        'user_blood_group',
        'searched_user_id',
        'searched_user_name',
        'searched_user_contact_no',
        'searched_user_blood_group',
    ];
}
