<?php

use App\Http\Controllers\AboutUsController;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\isAuthenticated;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserActivityController;
use App\Http\Controllers\UserController;

//----------------  Auth Apis  ----------------//

Route::prefix('auth')->group(function () {

    Route::post('register', [AuthController::class , 'register']);
    Route::post('login', [AuthController::class , 'login']);
    Route::put('verify_user', [AuthController::class , 'VerifyUser']);
    Route::put('reset_password/{id}', [AuthController::class , 'ResetPassword']);
});

//----------------  User Apis  ----------------//

Route::middleware([isAuthenticated::class])->group(function () {


    Route::apiResource('user', UserController::class);
    Route::get('my_profile',[UserController::class,'myProfile']);

    Route::patch('set_donor_status/{id}',[UserController::class,'setDonorStatus']);
    Route::patch('change_password',[UserController::class,'changePassword']);


    //----------------  User Activity Apis  ----------------//

    Route::put('create_data',[UserActivityController::class,'setUserActivity']);
    Route::get('user_activity',[UserActivityController::class,'getUserActivity']);

     //----------------  About Us Apis  ----------------//
    Route::get('aboutUsPageDetails',[AboutUsController::class,'aboutUsPageDetails']);



});
