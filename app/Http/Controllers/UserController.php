<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Rules\MatchOldPassword;
use App\Rules\ShouldNotMatchOldPassword;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{


    private $blood_group = ['','o-','o+','b-','b+','a-','a+','ab-','ab+'];

    private $get = ['id', 'name', 'email', 'contact_no', 'dob', 'blood_group','blood_group_id', 'address', 'city', 'pin_code','is_profile_updated'];

    public function index(Request $req)
    {

        $user = Auth::user()->id;

        $user_pin_code =  Auth::user()->pin_code;
        $user_relative_pin_code =  intval((Auth::user()->pin_code)/1000);

        $relative_pin_code = [ $user_relative_pin_code, $user_relative_pin_code-1, $user_relative_pin_code+1 ];

        if($req->blood_group_id){

            $q1 =  User::where(function ($q) use ($req, $relative_pin_code,$user) {
                $q->where('users.blood_group_id', $req->blood_group_id)
                ->where('users.is_donor', 1)
                ->whereNotIn('id', [$user])
                ->where('pin_code', 'like', '%' . $relative_pin_code[0] . '%');
            });

            $q2 =  User::where(function ($q) use ($req, $relative_pin_code,$user) {
                $q->where('users.blood_group_id', $req->blood_group_id)
                ->where('users.is_donor', 1)
                ->whereNotIn('id', [$user])
                ->where('pin_code', 'like', '%' . $relative_pin_code[1]. '%');
            });

            $q3 =  User::where(function ($q) use ($req, $relative_pin_code,$user) {
                $q->where('users.blood_group_id', $req->blood_group_id)
                ->where('users.is_donor', 1)
                ->whereNotIn('id', [$user])
                ->where('pin_code', 'like', '%' . $relative_pin_code[2]. '%');
            })->union($q1)->union($q2);


           return $q3->whereNotIn('id', [$user])->orderByRaw('pin_code = ? desc',$user_pin_code)->whereNotNull('blood_group_id')->paginate(10);
        }

        $q1 =  User::where('pin_code', 'like', '%' . $relative_pin_code[0] . '%')->where('is_donor', 1)->whereNotIn('id', [$user]);
        $q2 =  User::where('pin_code', 'like', '%' . $relative_pin_code[1] . '%')->where('is_donor', 1)->whereNotIn('id', [$user]);
        $q3 =  User::where('pin_code', 'like', '%' . $relative_pin_code[2] . '%')->where('is_donor', 1)->whereNotIn('id', [$user])->union($q1)->union($q2);

       return $q3->orderByRaw('pin_code = ? desc',$user_pin_code)->whereNotNull('blood_group_id')->paginate(10);

    }

    public function update(Request $req, $id)
    {

        $details = $req->validate([
            'name' => 'required',
            'email' => 'nullable|unique:users,email,' . $id,
            'contact_no' => 'required|unique:users,contact_no,' . $id,
            'dob' => 'required|before:today',
            'blood_group' => 'nullable',
            'blood_group_id' => 'required',
            'address' => 'required',
            'city' => 'required',
            'pin_code' => 'required',
        ]);

        $details['blood_group'] = $this->blood_group[$details['blood_group_id']];
        $details['is_profile_updated'] = true;

        User::find($id)->update($details);
        return User::find($id);
    } 

    public function myProfile(Request $req)
    {
        return $req->user();
    }

    public function show($id)
    {
        $user = User::find($id);
        $user->dob = Carbon::parse($user->dob)->diff(Carbon::now())->format('%y');
        return $user;
    }

    public function changePassword(Request $req){

        $data = $req->validate([
            'password' =>  ['required', 'min:6', new MatchOldPassword ],
            'new_password' => ['required', 'min:6', new ShouldNotMatchOldPassword ],
            'confirm_new_password' => 'required|same:new_password'
        ]);

        $user = $req->user();
        $newHashedPassword = Hash::make($data['new_password']);
        $user->password = $newHashedPassword;
        $user->save();
        return $user;
    }


    public function setDonorStatus(Request $req,$id){

        if($req->is_donor){
            $status = '1';
        } else {
            $status = '0';
        }

        User::find($id)->update(['is_donor' => $status]);
        return User::find($id);
    }

    public function setAdminStatus(Request $req)
    {
        $detail = $req->validate([
            'contact_no' =>  'required|min:10|exists:users,contact_no',
            'status' => 'required'
        ]);

        return User::where('contact_no',$detail['contact_no'])->update(['is_admin' => $detail['status']]);
    }


}
