<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function aboutUsPageDetails(){


        $details = [
            0 => [
                'heading' => '<b>Mission</b>',
                'description' => 'Connect Lives | Share Life',
            ],
            1 => [
                'heading' => '<b>Vision</b>',
                'description' => 'To serve as the trusted healthcare partner for patient who need blood in emergency.',
            ],
            2=> [
                'heading' => '<b>Community Leader</b>',
                'description' => 'This app is owned by <b> Mr. Nilesh Jaiswal</b>',
            ],
            3 => [
                'heading' => '<b>For any query please feel free to contact</b>',
                'description' => "<a href='tel:9424813105'>+91-94248-13105</a>",
            ],
        ];

        return response()->json($details);
    }
}
