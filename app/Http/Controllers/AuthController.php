<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    public function register(Request $req){

        // return $req;
        $data = $req->validate([
            'name' => 'required',
            'contact_no' => 'required|min:10|unique:users',
            'password' => 'required|min:6',
        ]);

        $data['password'] = Hash::make($data['password']);

        // return $data;
        $user = User::create($data);

        $token = Auth::attempt($req->only(['contact_no', 'password']));

        if ($token ) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);

    }

    public function login(Request $req){

        $credentials = $req->validate([
            'contact_no' => 'required|min:10|exists:users,contact_no',
            'password' => 'required|min:6',
        ]);

        $token = Auth::attempt($credentials);

        if ($token ) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);

    }


    public function VerifyUser(Request $req)
    {

        $credentials = $req->validate([
            'contact_no' => 'required|min:10|exists:users,contact_no',
            'dob' => 'required|before:today',
        ]);

        $user = User::where('contact_no', $credentials['contact_no'])->first();

        if( $user->dob == $credentials['dob']){
            return $user;
        }

        return response()->json(['error' => 'Inavlid Details'], 422);
    }


    public function ResetPassword(Request $req,$id){

        $data = $req->validate([
            'password' => 'required|min:6' ,
            'confirm_password' => 'required|same:password'
        ]);

        $newHashedPassword = Hash::make($data['password']);
        $user = User::find($id)->update(['password' => $newHashedPassword]);
        return $user;
    }


     /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            // 'refresh_token' => Auth::refresh(),
            'user' => Auth::user()
        ]);
    }
}
