<?php

namespace App\Http\Controllers;

use App\Models\UserActivityDetail;
use Illuminate\Http\Request;

class UserActivityController extends Controller
{

    public function setUserActivity(Request $req){

        $details = $req->validate([
            'user_id' => 'required',
            'user_name' => 'required',
            'user_contact_no' => 'required',
            'user_blood_group' => 'required',
            'searched_user_id' => 'required',
            'searched_user_name' => 'required',
            'searched_user_contact_no' => 'required',
            'searched_user_blood_group' => 'required',

        ]);

        $user = UserActivityDetail::create($details);
        return $user;
    }


    public function getUserActivity(){
        return UserActivityDetail::latest()->paginate(10);
    }
}
