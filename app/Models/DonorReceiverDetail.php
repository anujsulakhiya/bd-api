<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorReceiverDetail extends Model
{
    use HasFactory;


    protected $fillable = [
        'donor_id',
        'donor_email',
        'donor_contact_no',
        'receiver_id',
        'receiver_email',
        'receiver_contact_no',
        'donor_contact_clicked',
    ];
}
