<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin User',
            'email' => 'admin@bd.com',
            'password' => Hash::make('123456'),
            'blood_group' => 'a+',
            'blood_group_id' => 6,
            'address' => 'Betma',
            'city' => 'Indore',
            'pin_code' => '453001',
            'contact_no' => '1234567890',
            'is_profile_updated' => 1,
            'is_donor' => 1,
        ]);

        $data = [];
        $j=0;
        for($i=0;$i <= 10; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'a+',
                'blood_group_id' => 6,
                'address' => 'Betma',
                'city' => 'Indore',
                'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
            $j++;
        }
        $j=0;
        for($i=11;$i <= 21; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'a-',
                'blood_group_id' => 5,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=22;$i <= 31; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'b+',
                'blood_group_id' =>4,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=32;$i <= 41; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'b-',
                'blood_group_id' => 3,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=42;$i <= 51; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'ab+',
                'blood_group_id' => 8,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=62;$i <= 71; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'ab-',
                'blood_group_id' => 7,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=72;$i <= 81; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'o+',
                'blood_group_id' => 2,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=82;$i <= 91; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'o-',
                'blood_group_id' => 1,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=0;$i <= 10; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'a+',
                'blood_group_id' => 6,
                'address' => 'Betma',
                'city' => 'Indore',
                'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
            $j++;
        }
        $j=0;
        for($i=11;$i <= 21; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'a-',
                'blood_group_id' => 5,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=22;$i <= 31; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'b+',
                'blood_group_id' =>4,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=32;$i <= 41; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'b-',
                'blood_group_id' => 3,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
                'is_profile_updated' => 1,
                'is_donor' => 1,

            ];
$j++;
        }
        $j=0;
        for($i=42;$i <= 51; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'ab+',
                'blood_group_id' => 8,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=62;$i <= 71; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'ab-',
                'blood_group_id' => 7,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=72;$i <= 81; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'o+',
                'blood_group_id' => 2,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=82;$i <= 91; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'o-',
                'blood_group_id' => 1,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }


        $j=0;
        for($i=92;$i <= 101; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'a+',
                'blood_group_id' => 6,
                'address' => 'Betma',
                'city' => 'Indore',
                'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
            $j++;
        }
        $j=0;
        for($i=102;$i <= 111; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'a-',
                'blood_group_id' => 5,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=112;$i <= 121; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'b+',
                'blood_group_id' =>4,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=122;$i <= 131; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'b-',
                'blood_group_id' => 3,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=132;$i <= 141; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'ab+',
                'blood_group_id' => 8,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=142;$i <= 151; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'ab-',
                'blood_group_id' => 7,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=152;$i <= 161; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'o+',
                'blood_group_id' => 2,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }
        $j=0;
        for($i=162;$i <= 171; $i++){

            $data[$i-2] =  [
                'name' => "user$i",
                'email' => "faculty$i@cm.com",
                'password' => '$2y$10$D8ggyjcZHAVNGDATg4Y2NuppI8qjS5PVxMVimghmS3ecWH97KIncy', //123456
                'blood_group' => 'o-',
                'blood_group_id' => 1,
                'address' => 'Betma',
                'city' => 'Indore',
                 'pin_code' => '45'.$j.'001',
                'contact_no' => rand(1111111111,9999999999),
                'is_profile_updated' => 1,
                'is_donor' => 1,
            ];
$j++;
        }

        User::insert($data);
    }
}
